use std::ffi::{OsStr, OsString};
use std::io::{self, BufRead};
use std::os::unix::ffi::OsStrExt;

pub trait BufReadExt: io::BufRead {
    fn os_strings(self) -> OsStrings<Self>
    where
        Self: Sized,
    {
        OsStrings { buf: self }
    }
}

impl<B: io::BufRead> BufReadExt for B {}

#[derive(Debug)]
pub struct OsStrings<B> {
    buf: B,
}

impl<B: BufRead> Iterator for OsStrings<B> {
    type Item = io::Result<OsString>;

    fn next(&mut self) -> Option<io::Result<OsString>> {
        let mut buf = Vec::new();
        match self.buf.read_until(b'\n', &mut buf) {
            Err(e) => Some(Err(e)),
            Ok(0) => None,
            Ok(_) => {
                let osstr = OsStr::from_bytes(&buf[..buf.len() - 1]);
                Some(Ok(osstr.to_os_string()))
            }
        }
    }
}
