use crate::cli::Course;
use crate::cli::SortingMode;
use crate::file::File;

pub fn sort(pool: &mut [File], srtmode: &SortingMode) {
    match srtmode {
        SortingMode::Abc(Course::Forward) => sort_abc_forward(pool),
        SortingMode::Abc(Course::Backward) => sort_abc_backward(pool),
        SortingMode::Mtime(Course::Forward) => sort_mtime_forward(pool),
        SortingMode::Mtime(Course::Backward) => sort_mtime_backward(pool),
        SortingMode::Size(Course::Forward) => sort_size_forward(pool),
        SortingMode::Size(Course::Backward) => sort_size_backward(pool),
    }
}

fn sort_abc_forward(pool: &mut [File]) {
    pool.sort_unstable_by(|a, b| alphanumeric_sort::compare_path(&a.path, &b.path));
}

fn sort_abc_backward(pool: &mut [File]) {
    pool.sort_unstable_by(|a, b| alphanumeric_sort::compare_path(&b.path, &a.path));
}

fn sort_mtime_forward(pool: &mut [File]) {
    pool.sort_unstable_by(|a, b| a.mtime().cmp(&b.mtime()));
}

fn sort_mtime_backward(pool: &mut [File]) {
    pool.sort_unstable_by(|a, b| b.mtime().cmp(&a.mtime()));
}

fn sort_size_forward(pool: &mut [File]) {
    pool.sort_unstable_by(|a, b| a.size().cmp(&b.size()));
}

fn sort_size_backward(pool: &mut [File]) {
    pool.sort_unstable_by(|a, b| b.size().cmp(&a.size()));
}
