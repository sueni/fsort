use std::error::Error;
use std::ffi::OsString;
use std::fmt;

pub const HELP: &str = "
USAGE:
      PATHS | fsort MODE

MODES:
      abc-frw, abc-bkwr, mtime-frw, mtime-bkwr, size-frw, size-bkwr
";

pub enum Course {
    Forward,
    Backward,
}

#[derive(Debug)]
pub struct SortingModeError;
impl Error for SortingModeError {}
impl fmt::Display for SortingModeError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Invalid sorting mode")
    }
}

pub enum SortingMode {
    Abc(Course),
    Mtime(Course),
    Size(Course),
}

impl TryFrom<Option<OsString>> for SortingMode {
    type Error = SortingModeError;

    fn try_from(value: Option<OsString>) -> Result<Self, Self::Error> {
        match value {
            Some(mode) if mode == *"abc-frw" => Ok(Self::Abc(Course::Forward)),
            Some(mode) if mode == *"abc-bkwr" => Ok(Self::Abc(Course::Backward)),
            Some(mode) if mode == *"mtime-frw" => Ok(Self::Mtime(Course::Forward)),
            Some(mode) if mode == *"mtime-bkwr" => Ok(Self::Mtime(Course::Backward)),
            Some(mode) if mode == *"size-frw" => Ok(Self::Size(Course::Forward)),
            Some(mode) if mode == *"size-bkwr" => Ok(Self::Size(Course::Backward)),
            _ => Err(SortingModeError),
        }
    }
}

pub fn parse_args() -> Result<SortingMode, SortingModeError> {
    let mut args = std::env::args_os().skip(1);
    args.next().try_into()
}
