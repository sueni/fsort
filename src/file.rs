use std::path::{Path, PathBuf};
use std::time::SystemTime;

pub struct File {
    pub path: PathBuf,
}

impl File {
    pub fn new(path: impl AsRef<Path>) -> Option<Self> {
        let path = PathBuf::from(path.as_ref());
        if path.is_file() {
            Some(Self { path })
        } else {
            None
        }
    }

    pub fn mtime(&self) -> SystemTime {
        let metadata = self.path.metadata().unwrap();
        metadata.modified().unwrap()
    }

    pub fn size(&self) -> u64 {
        let metadata = self.path.metadata().unwrap();
        metadata.len()
    }
}
