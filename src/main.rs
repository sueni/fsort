use std::io::{self, BufReader, BufWriter, Write};
use std::os::unix::ffi::OsStrExt;

mod cli;
mod file;
mod io_ext;
mod sorter;

use file::File;
use io_ext::BufReadExt;

pub fn display(files: &[File]) -> io::Result<()> {
    let mut writer = BufWriter::with_capacity(64 * 1024, io::stdout());
    for file in files {
        writer.write_all(file.path.as_os_str().as_bytes())?;
        writer.write_all(b"\n")?;
    }
    Ok(())
}

fn main() {
    let srtmode = match cli::parse_args() {
        Ok(srtmode) => srtmode,
        Err(err) => {
            eprintln!("ERROR: {}\n{}", err, cli::HELP);
            std::process::exit(1);
        }
    };

    let reader = BufReader::with_capacity(64 * 1024, io::stdin());
    let mut files: Vec<File> = reader
        .os_strings()
        .flatten()
        .filter_map(File::new)
        .collect();

    sorter::sort(&mut files, &srtmode);
    display(&files).unwrap();
}
